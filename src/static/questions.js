[
  {
    label: "Rozdział 1",
    category: "Postanowienia ogólne",
    question: "Które jednostki organizacyjne ZHP posiadają osobowość prawną?",
  },
  {
    label: "Rozdział 2",
    category: "Charakter, cele i środki działania",
    question: "Co jest misją ZHP?",
  },
  {
    label: "Rozdział 3",
    category: "Hymn, odznaki, symbole, mundury organizacyjne",
    question: "Co jest hymnem ZHP?",
  },
  {
    label: "Rozdział 4",
    category: "Członostwo",
    question:
      "Osoby w jakim wieku potrzebują pisemnej zgody rodziców lub opiekunów na przynależnośc do ZHP?",
  },
  {
    label: "Rozdział 5",
    category: "Struktura",
    question: "Czyim rozkazem mianuje się drużynowego?",
  },
  {
    label: "Rozdział 6",
    category: "Władze, podejmowanie uchwał i decyzji, wybory władz",
    question: "Ile trwa kadencja władz ZHP?",
  },
  {
    label: "Rozdział 7",
    category: "Władze hufca",
    question: "Kto lub co jest najwyższą władzą hufca?",
  },
  {
    label: "Rozdział 8",
    category: "Władze chorągwi",
    question: "Co najmniej ilu członków liczy komenda chorągwi?",
  },
  {
    label: "Rozdział 9",
    category: "Władze naczelne",
    question: "Kto kieruje działalnością Rady Naczelnej ZHP?",
  },
  {
    label: "Rozdział 10",
    category: "Majątek, gospodarka",
    question: "Do czego służy majątek ZHP?",
  },
  {
    label: "Rozdział 11",
    category: "Postanowienia końcowe",
    question: "Kto może zmienić statut ZHP?",
  },
];
